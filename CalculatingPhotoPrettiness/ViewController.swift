//
//  ViewController.swift
//  CalculatingPhotoPrettiness
//
//  Created by Rakesh Sharma on 24/12/20.
//  Copyright © 2020 Shaadi. All rights reserved.
//

import UIKit
import MyFirstSwiftLib
class ViewController: UIViewController {

    @IBOutlet weak var userImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let triggerTime = (Int64(NSEC_PER_SEC) * 10)
       DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
            self.findPhotoPrettinessPressed(UIButton())
        }
    }


    @IBAction func findPhotoPrettinessPressed(_ sender: Any) {
        let prettiness  = PhotoPrettinessCalculator.findPhotoPrettiness(image: self.userImageView.image!)
        let alert = UIAlertController(title: "Your Photos Prettines Level is :-" + prettiness, message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

